import React, { Component } from 'react';
import { StyleSheet, ScrollView, Text, View, Image, TouchableOpacity } from 'react-native';
import { Button, Flex, WhiteSpace, WingBlank, InputItem } from '@ant-design/react-native';
import { connect } from 'react-redux'
import axios from 'axios';
import { push } from 'connected-react-router'
class signin extends Component {

  state = {
    visible: false,
    email: '',
    password: '',
    firstname: '',
    lastname: '',
  }

  onClickSignup = () => {
    // const { push } = this.props
    // push('/signup')
    this.props.history.push('/signup')

  }

  onClickSignin = () => {
    // const { push } = this.props
    // push('/listmovies')
    this.props.history.push('/listmovies')
  }

  // onChangeValue = (index, value) => this.setState({ [index]: value })
  onSignin = () => {
    this.setState({ visible: true })
    axios({
      url: 'https://zenon.onthewifi.com/ticGo/users/login?email',
      method: 'post',
      data: {
        // email:'test2@gmail.com',
        // password:'123456'
        email: this.state.email,
        password: this.state.password,
      }
    }).then(res => {
      const { data } = res
      const { user } = data
      this.props.addUser(data);
      this.props.history.push('/listmovies')

    }).catch(e => {
      console.log("error " + JSON.stringify(e))
      console.log("asdosadjas")
      this.setState({ visible: false })
      alert(e.response.data.errors.email)
    })
    
    
  }




  render() {
    const { username, password } = this.state
    const { user, addUser } = this.props
    return (
      <View style={styles.content}>

        <WingBlank style={{ marginBottom: 5 }}>


          <Flex style={[styles.center]}>
            <Image source={require('./images/logo.png')} style={[styles.imagesize]} />
          </Flex>
          <Text style={styles.textHead}>SIGN IN</Text>

          <InputItem
            type="text"
            placeholder="Email"
            value={this.state.email}
            onChange={email => {
              this.setState({
                email,
              });
            }}

          />
          <InputItem
            type="password"
            placeholder="Password"
            secureTextEntry={true}
            value={this.state.password}
            onChange={password => {
              this.setState({
                password,
              });
            }}

          />
          <WingBlank style={{ margin: 20 }}>
            <Button  
              loading={this.state.visible}
              type='primary' style={[styles.button]}
              onPress={() => this.onSignin()}
              activeStyle={{ backgroundColor: '#f47373' }}>
              SIGN IN
          </Button>
            <TouchableOpacity onPress={this.onClickSignup}>
              <Text style={{ textAlign: 'center', fontSize: 15, margin: 10, color: '#3f51b5' }}>Sign up for TicGo</Text>
            </TouchableOpacity>
          </WingBlank>
        </WingBlank>
      </View>
    );
  }
}

const mapStateToProps = ({ user }) => ({
  username: user.username,
  password: user.password,
})

const mapDidpatchToProps = (dispatch) => {
  return {
    addUser: (user) => {
      dispatch({
        type: 'ADD_USER',
        user: user
      })
    }
  }
}

export default connect(mapStateToProps, mapDidpatchToProps)(signin)

const styles = StyleSheet.create({
  content: {
    flex: 1,
    // alignItems: 'center',
    justifyContent: 'center'
  },
  imagesize: {
    width: 200,
    height: 200
  },
  center: {
    alignItems: 'center',
    justifyContent: 'center'
  },
  button: {
    backgroundColor: '#f47373',
    borderColor: '#f47373'
  },
  textHead: {
    textAlign: 'center',
    fontSize: 25,
    color: 'black',
    marginBottom: 10
  },
});