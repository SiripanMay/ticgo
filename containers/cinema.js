import React, { Component } from 'react';
import { Text, View, StyleSheet, Image, TouchableOpacity, ScrollView, ActivityIndicator, ListItem, Alert, FlatList, ImageBackground } from 'react-native';
import { SearchBar, TabBar, Tabs, List, Flex, Grid, ListView, Button, Carousel } from '@ant-design/react-native';
import axios from 'axios'
import { Icon } from 'react-native-elements'
import { connect } from 'react-redux'
var options = { weekday: 'short', month: 'short', day: 'numeric' };

// console.log(today.toLocaleDateString("en-US")); // 9/17/2016
// console.log(today.toLocaleDateString("en-US", options)); // Saturday, September 17, 2016

var tab1 = new Date();
date1 = tab1.getTime()
day1 = tab1.toLocaleDateString("en-US", options)
var tab2 = new Date(new Date().getTime() + 24 * 60 * 60 * 1000)
date2 = tab2.getTime()
day2 = tab2.toLocaleDateString("en-US", options)
var tab3 = new Date(new Date().getTime() + 48 * 60 * 60 * 1000)
date3 = tab3.getTime()
day3 = tab3.toLocaleDateString("en-US", options)


class cinema extends Component {

    onClickShowing = () => {
        this.props.history.push('/listmovies')

    }
    onClickProfile = () => {
        this.props.history.push('/profile')

    }

    onClickBack = (item) => {
        console.log(item);

        this.props.history.push('/detailBooking', { item: item })

    }

    onClickProfile = () => {
        this.props.history.push('/profile')
    }
    onClickBookingHistory = () => {
        this.props.history.push('/bookingHistory')

    }
    constructor(props) {
        super(props);
        this.state = {
            selectedTab: 'Showing',
            cinema: [],
            items: [],
            isLoading: true,
            movies: [],
            isLoadingShowing: true,
            seats: [],
            tabs: [
                { title: day1 },
                { title: day2 },
                { title: day3 },
            ],
            setSeats: [],
            booking: [],
            price:[],
            data:[]

        };
    }

    onChangeTab() {
        this.setState({
            selectedTab: tabName,
        });
    }



    renderSeat = (item) => {

        return (
            <View style={{ flex: 1 }}>

                {item.item.type === "DELUXE" ? (
                    <View style={{ marginLeft: 5, alignItems: 'center', justifyContent: 'center' }}>
                        <Icon type='material-community' name='seat' color='#F7B3B3' />
                        <Text>Deluxe</Text>
                        <Text>{item.item.price}฿ </Text>
                    </View>
                ) : (
                        <View></View>

                    )}

                {item.item.type === "PREMIUM" ? (
                    <View style={{ marginLeft: 5, alignItems: 'center', justifyContent: 'center' }}>
                        <Icon type='material-community' name='seat' color='#f47373' />
                        <Text>Premium</Text>
                        <Text>{item.item.price}฿ </Text>

                    </View>
                ) : (
                        <View></View>

                    )}


                {item.item.type === "SOFA" ? (

                    <View style={{ marginLeft: 5, alignItems: 'center', justifyContent: 'center' }}>
                        <Icon type='material-community' name='sofa' color='#F40808' />
                        <Text>Sofa Sweet (Pair)</Text>
                        <Text>{item.item.price}฿ </Text>
                    </View>

                ) : (
                        <View></View>

                    )}

            </View>


        )


    }

    UNSAFE_componentWillMount() {
        this.renderS()
    }

    renderS = () => {
        const item = this.props.location.state.item
        let items = item.seats
        console.log(items);
        items = items.map(state => {
            return Object.keys(state).reduce((sum, each) => {
                sum[each] = state[each]
                console.log('sum', sum);
                console.log('each', each);
                console.log('sum[each]', sum[each]);
                console.log('state[each]', state[each]);
                console.log('state', state);

                if (each.includes('row') && !each.includes('rows')) {
                    console.log('summmmmm', sum[each]);
                    sum[each] = sum[each].map(seat => seat ? 'b' : 'f')
                    console.log('summmmmm', sum[each]);

                }

                return sum

            }, {})
        })
        this.setState({ items: items })
    }
    pressSeat = (type, row, column, price) => {
        this.state.booking.push({ column: (column+1), row: (row+1), type: type })
        this.state.data.push({ C: (column+1), R1: (row+1), type: type ,price:price})
        this.state.price.push(price)  
        const items = this.state.items
        let item = items
        item = item.map(i => {
            if (i.type !== type) {
                return i
            }return Object.keys(i).reduce((sum, each) => {
                sum[each] = i[each]
                if (each.includes('row') && !each.includes('rows')) {
                    sum[each] = sum[each].map((item, index) => {
                        if (each === `row${row + 1}` && index === column  && item === 'f') {                    
                            return 'c'
                        }
                        else {
                            if (each === `row${row + 1}` && index === column && item === 'c') {
                                return 'f'
                            } else {
                                return item
                            }
                        }
                    })
                }
                return sum
            }, {})
        })

        this.setState({ items: item })
    }

    Booking = () => {
        const { user } = this.props
        
        console.log(this.props.location.state.item._id);
        console.log(this.state.booking);
        

        //alert('Booking is Success')
        
        axios({
            method: 'post',
            url: 'https://zenon.onthewifi.com/ticGo/movies/book',
            headers: { 'Authorization': `bearer ${user[0].user.token}` },
            data: {
                showtimeId: this.props.location.state.item._id,
                seats: this.state.booking
            }
        })
            .then(() => {
                alert('Booking is Success')
            })
            .catch((error) => {
                console.log(error,JSON.stringify(error));
                alert('Please Select Seats')
            })

    }


    render() {
        const item = this.props.location.state.item
        var date = new Date(item.startDateTime)
        var conDate = date.toDateString()
        var conTime = new Date(item.startDateTime).toLocaleTimeString().replace(/(.*)\D\d+/, '$1')
        // console.log('date' + conDate);
        // console.log(this.state.movies);
        // console.log(item.seats);
        console.log('cinema: ' + this.state.cinema);
        console.log('aaaaaaaaaaaaaaaaa', this.state.items);

        return (



            <TabBar
                unselectedTintColor="#949494"
                tintColor="#f47373"
                barTintColor="#f5f5f5"
            >
                <TabBar.Item
                    title="Showing"
                    icon={<Icon type='material-community' name="youtube" />}
                    selected={this.state.selectedTab === 'Showing'}
                    onPress={() => this.onClickShowing()}
                >
                    <View style={[styles.content]}>

                        <View style={[styles.boxHeader]}>

                            <TouchableOpacity
                                onPress={() => this.onClickBack(this.props.location.state.name)}
                            >
                                <View style={[styles.rowIcon]}>
                                    <Icon type='material-community' name='chevron-left' />
                                </View>
                            </TouchableOpacity>

                            <View style={[styles.rowHeader]}>
                                <Text style={[styles.textHead]}>Select Seat</Text>
                            </View>

                        </View>

                        <ScrollView style={{ flex: 1 }}>
                            <View style={[styles.boxContent]}>

                                <View style={[styles.header, styles.center]}>

                                    <View style={{ margin: 10 }}>
                                        <Image source={{ uri: item.movie.image }}
                                            style={{ width: 50, height: 70 }} />
                                    </View>


                                    <View style={[styles.center]}>
                                        <Text style={styles.textmovie}>
                                            {conDate} | {conTime}
                                        </Text>
                                        <Text style={styles.textmovie}>
                                            {item.movie.name}
                                        </Text>
                                        <View style={{ flexDirection: 'row', margin: 3 }}>
                                            <Text style={styles.textmovie}>
                                                {item.cinema.name}
                                            </Text>

                                            <Icon type='material-community' name='volume-high' />
                                            <Text style={styles.textmovie}>{item.soundtrack}</Text>
                                        </View>
                                    </View>
                                </View>

                                <View style={[styles.box1, styles.center]}>
                                    <View style={[styles.box1, styles.center]}>

                                        <FlatList
                                            data={item.cinema.seats}
                                            numColumns={3}
                                            renderItem={(item) => this.renderSeat(item)}

                                        />

                                    </View>
                                </View>
                                <View style={[styles.boxContent]}>

                                    <View style={[styles.box2, styles.center]}>

                                        <View style={{ flex: 1, margin: 10, flexDirection: 'column', alignItems: 'center' }}>
                                            <View style={{ margin: 5, alignItems: 'center' }}>
                                                <Image source={require('./images/screen.jpg')} style={{ width: 300, height: 35 }} />
                                            </View>

                                            {/* {this.renderS()} */}
                                            {this.state.items.map(i => {
                                                console.log('i', i);

                                                return Object.keys(i).map((row, rowIndex) => {

                                                    return (
                                                        <FlatList
                                                            data={i['row' + (rowIndex + 1)]}
                                                            numColumns={i.columns}
                                                            renderItem={({ item, index }) => {
                                                                
                                                                if (item === 'f') {
                                                                    if (i.type === 'SOFA') {
                                                                        return (
                                                                            <View>
                                                                                <TouchableOpacity onPress={() => this.pressSeat(i.type, (rowIndex), (index), i.price)}>
                                                                                    <View style={{ margin: 2 }}>
                                                                                        <Icon type='material-community' name='sofa' color='#F40808' size={16} />
                                                                                    </View>
                                                                                </TouchableOpacity>
                                                                            </View>
                                                                        )

                                                                    } else if (i.type === "PREMIUM") {
                                                                        return (
                                                                            <TouchableOpacity onPress={() => this.pressSeat(i.type, (rowIndex), (index), i.price)}>
                                                                                <View>
                                                                                    <Icon type='material-community' name='seat' color='#f47373' size={14} />
                                                                                </View>
                                                                            </TouchableOpacity>
                                                                        )
                                                                    } else if (i.type === "DELUXE") {
                                                                        return (
                                                                            <TouchableOpacity onPress={() => this.pressSeat(i.type, (rowIndex), (index), i.price)}>
                                                                                <View>
                                                                                    <Icon type='material-community' name='seat' color='#F7B3B3' size={14} />
                                                                                </View>
                                                                            </TouchableOpacity>
                                                                        )
                                                                    }

                                                                } else if (item === 'c') {
                                                                    return (
                                                                         <TouchableOpacity onPress={() => this.pressSeat(i.type, (rowIndex), (index), i.price)}>
                                                                            <View>
                                                                                <Icon type='material-community' name='check-circle' color='red' size={14} />
                                                                            </View>
                                                                        </TouchableOpacity>
                                                                        
                                                                    )

                                                                } else if (item === 'b') {
                                                                    return (
                                                                       <View>
                                                                            <Icon type='material-community' name='account-circle-outline' color='#666666' size={14} />
                                                                        </View>
                                                                    )

                                                                }
                                                            }
                                                            }
                                                        />
                                                    )
                                                })

                                            }).reverse()
                                            }
                                        </View>
                                    </View>
                                </View>

                                <View style={[styles.box3, styles.center]}>
                                    <View style={[styles.box3, styles.center]}>
                                        
                                        <Text>Total : {this.state.price.reduce((x, y) => { return x + y }, 0)} THB</Text>
                                        
                                    </View>
                                    <View>
                                            <Button
                                            onPress={()=>this.Booking()}
                                            >Book Now</Button>
                                        </View>
                                </View>
                            </View>

                        </ScrollView>
                    </View>
                </TabBar.Item>



                <TabBar.Item
                    icon={<Icon type='material-community' name='heart' />}
                    title="Faverite"
                //selected={this.state.selectedTab === 'Faverite'}
                //onPress={() => this.onChangeTab('Faverite')}
                />

                <TabBar.Item
                    icon={<Icon type='material-community' name='history' />}
                    title="Booking History"
                    //selected={this.state.selectedTab === 'Booking'}
                    onPress={() => this.onClickBookingHistory()}
                />


                <TabBar.Item
                    icon={<Icon type='material-community' name='account' />}
                    title="Profile"
                    //selected={this.state.selectedTab === 'Profile'}
                    onPress={() => this.onClickProfile()}
                />
            </TabBar>

        );
    }
}

const mapStateToProps = (state) => {
    return {
        user: state.user
    }
}
const mapDispatchToProps = (dispatch) => {
    return {
        saveUserInRedux: ({ firstName, lastName }) => {
            dispatch({
                type: 'EDIT_USER',
                firstName: firstName,
                lastName: lastName
            })
        }
    }
}

  
  export default connect(mapStateToProps, mapDispatchToProps)(cinema)

const styles = StyleSheet.create({
    content: {
        flex: 1,
        // alignItems: 'center',
        // justifyContent: 'center'
    },
    imagesize: {
        width: '100%',
        height: 30,
        margin: 0

    },
    center: {
        alignItems: 'center',
        justifyContent: 'center'
    },
    button: {
        backgroundColor: '#f47373',
        borderColor: '#f47373'
    },
    textHead: {
        textAlign: 'center',
        fontSize: 15,
        color: 'black',

    },
    textmovie: {
        textAlign: 'center',
        fontSize: 15,
        color: 'black',

    },
    boxHeader: {
        flexDirection: 'row',
        backgroundColor: '#f5f5f5',
        borderBottomWidth: 3,
        borderBottomColor: '#f47373',
        justifyContent: 'center',
        padding: 15,
    },
    boxContent: {
        flexDirection: 'column',
        backgroundColor: 'white',
        justifyContent: 'center',
        //margin: 10

    },
    rowHeader: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
    rowIcon: {
        flex: 0,
        width: 20
    },
    box1: {
        flex: 1,
        flexDirection: 'row',
        height: 80,
        width: '100%',
        backgroundColor: '#f5f5f5'

    },
    box2: {
        flex: 1,
        flexDirection: 'row',
        height: 260,
        width: '100%',
        backgroundColor: 'white'
    },
    box3: {
        flex: 1,
        flexDirection: 'column',
        height: 70,
        width: '100%',
        backgroundColor: '#f5f5f5'
    },
    header: {
        flex: 1,
        flexDirection: 'row',
        height: 100,
        width: '100%',
        backgroundColor: 'white'
    },


});