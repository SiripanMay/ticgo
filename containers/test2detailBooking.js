import React, { Component } from 'react';
import { Text, View, StyleSheet, Image, TouchableOpacity, ScrollView, ActivityIndicator, ListItem, Alert, FlatList, ImageBackground } from 'react-native';
import {  SearchBar, TabBar, Tabs, List, Flex, Grid, ListView, Button, Carousel } from '@ant-design/react-native';
import axios from 'axios'
import { Icon } from 'react-native-elements'
var options = { weekday: 'short', month: 'short', day: 'numeric' };

// console.log(today.toLocaleDateString("en-US")); // 9/17/2016
// console.log(today.toLocaleDateString("en-US", options)); // Saturday, September 17, 2016

var tab1 = new Date();
date1 = tab1.getTime()
day1 = tab1.toLocaleDateString("en-US", options)
var tab2 = new Date(new Date().getTime() + 24 * 60 * 60 * 1000)
date2 = tab2.getTime()
day2 = tab2.toLocaleDateString("en-US", options)
var tab3 = new Date(new Date().getTime() + 48 * 60 * 60 * 1000)
date3 = tab3.getTime()
day3 = tab3.toLocaleDateString("en-US", options)


class listmovies extends Component {

    onClickSignup = () => {
        this.props.history.push('/signup')
    }
    onClickBack = () => {
        this.props.history.push('/listmovies')

    }

    onClickProfile = () => {
        this.props.history.push('/profile')
    }
    constructor(props) {
        super(props);
        this.state = {
            selectedTab: 'Showing',
            items: [],
            isLoading: true,
            movies1: [],
            movies2: [],
            movies3: [],
            isLoadingShowing: true,
            tabs: [
                { title: day1 },
                { title: day2 },
                { title: day3 },
            ],
            dates: [
                { title: date1 },
                { title: date2 },
                { title: date3 },
            ]
            // tabs: [
            //     { title: day1, date: tab1 },
            //     { title: day2, date: tab2 },
            //     { title: day3, date: tab3 },
            // ]

        };
    }

    onChangeTab() {
        this.setState({
            selectedTab: tabName,
        });
    }


    getMovie = () => {
        axios.get(`https://zenon.onthewifi.com/ticGo/movies/showtimes/movie/${this.props.location.state.item._id}`)
            .then(response => {
                this.setState({
                    items: response.data,
                    isLoadingShowing: false
                })
            })
            .catch(err => { console.log(err) })
            .finally(() => { console.log('Finally') })
    }

    getMovieByDate1 = () => {
        axios.get(`https://zenon.onthewifi.com/ticGo/movies/showtimes/movie/${this.props.location.state.item._id}`, {
            params: {
                date: date1
            }
        })
            .then(response => {
                this.setState({
                    movies1: response.data,
                    //isLoading: false
                })
            })
            .catch(err => { console.log(err) })
            .finally(() => { console.log('Finally') })
    }

    getMovieByDate2 = () => {
        axios.get(`https://zenon.onthewifi.com/ticGo/movies/showtimes/movie/${this.props.location.state.item._id}`, {
            params: {
                date: date2
            }
        })
            .then(response => {
                this.setState({
                    movies2: response.data,
                    // isLoading: false
                })
            })
            .catch(err => { console.log(err) })
            .finally(() => { console.log('Finally') })
    }

    getMovieByDate3 = () => {
        axios.get(`https://zenon.onthewifi.com/ticGo/movies/showtimes/movie/${this.props.location.state.item._id}`, {
            params: {
                date: date3
            }
        })
            .then(response => {
                this.setState({
                    movies3: response.data,
                    //isLoading: false
                })
            })
            .catch(err => { console.log(err) })
            .finally(() => { console.log('Finally') })
    }

    UNSAFE_componentWillMount() {
        console.log(this.props)
        if (this.props.location && this.props.location.state && this.props.location.state.idmovie) {
            Alert.alert('Your number is', this.props.location.state.idmovie + '')
        }

    }

    // renderContent = (dates) => {
    //     console.log(dates);
    //     var a = this.state.tabs
    //     var b = a.indexOf(dates)
    //     console.log(b);
    //     console.log(this.state.dates[b]);

    //     axios.get(`https://zenon.onthewifi.com/ticGo/movies/showtimes/movie/${this.props.location.state.item._id}`,
    //         { params: { date: this.state.dates[b] } }
    //     )
    //         .then(response => {
    //             this.setState({
    //                 movies: response.data,
    //             })
    //         })
    //         .catch((error) => {
    //             console.error(error);
    //         })
    //         .finally(() => { console.log('Finally') })

    //     console.log(this.state.movies);

    // }

    clickTime = (item) => {
        console.log(item);
        
        this.props.history.push('/cinema', { item: item })

    }


    componentDidMount() {
        this.getMovie()
        this.getMovieByDate1()
        this.getMovieByDate2()
        this.getMovieByDate3()
    }

    render() {
        const movie = this.props.location.state.item
        // var st = this.state.movies2.startDateTime
        // var set = st.sort()
        // console.log('b:'+st);
        // console.log('a'+set);
        console.log(this.state.movies2);



        return (


            <TabBar
                unselectedTintColor="#949494"
                tintColor="#f47373"
                barTintColor="#f5f5f5"
            >
                <TabBar.Item
                    title="Showing"
                    icon={<Icon type='material-community' name='youtube' />}
                    selected={this.state.selectedTab === 'Showing'}
                    onPress={() => this.onChangeTab('Showing')}
                >
                    <View style={[styles.content]}>

                        <View style={[styles.boxHeader]}>

                            <TouchableOpacity
                                onPress={this.onClickBack}
                            >
                                <View style={[styles.rowIcon]}>
                                <Icon type='material-community' name='arrow-left' />
                                </View>
                            </TouchableOpacity>

                            <View style={[styles.rowHeader]}>
                                <Text style={[styles.textHead]}>{movie.name}</Text>
                            </View>

                        </View>
                        <ScrollView style={{ flex: 1 }}>
                            <View style={[styles.boxContent]}>
                                <ImageBackground source={{ uri: movie.image }} style={[styles.header]} blurRadius={5}>

                                    <View style={{ flex: 1, margin: 15 }}>
                                        <Image source={{ uri: movie.image }}
                                            style={{ width: '100%', height: '100%' }} />
                                    </View>


                                    <View style={[styles.rowHeader, styles.center]}>
                                        <Text style={styles.textmovie}>
                                            {movie.name}
                                        </Text>
                                        <Text style={styles.textmovie}>
                                            <Icon name="clock-circle" size="md" color="#f47373" /> {movie.duration} min
                                        </Text>
                                    </View>


                                </ImageBackground>

                                <View style={[styles.box2]}>
                                    <Tabs
                                        tabs={this.state.tabs}

                                    >
                                        <View>
                                            <FlatList
                                                data={this.state.movies1}
                                                numColumns={5}
                                                renderItem={(item) => (

                                                    <Button style={{ margin: 5, width: 80 }}
                                                        onPress={() => this.clickTime(item.item)}
                                                    >
                                                        {new Date(item.item.startDateTime).toLocaleTimeString().replace(/(.*)\D\d+/, '$1')}
                                                    </Button >

                                                )}

                                            />
                                        </View>
                                        <View>
                                            <FlatList
                                                data={this.state.movies2}
                                                numColumns={5}
                                                renderItem={(item) => (

                                                    <Button style={{ margin: 5, width: 80 }}
                                                    onPress={()=>this.clickTime(item.item)}

                                                    >
                                                        {new Date(item.item.startDateTime).toLocaleTimeString().replace(/(.*)\D\d+/, '$1')}
                                                    </Button >

                                                )}

                                            />
                                        </View>
                                        <View>
                                            <FlatList
                                                data={this.state.movies3}
                                                numColumns={5}
                                                renderItem={(item) => (
                                                    //console.log(item)

                                                    <Button style={{ margin: 5, width: 80 }}
                                                        onPress={() => this.clickTime(item.item)}
                                                    >
                                                        {new Date(item.item.startDateTime).toLocaleTimeString().replace(/(.*)\D\d+/, '$1')}

                                                    </Button >

                                                )}

                                            />
                                        </View>

                                    </Tabs>

                                </View>
                            </View>

                        </ScrollView>
                    </View>
                </TabBar.Item>



                <TabBar.Item
                     icon={<Icon type='material-community' name='heart' />}
                    title="Faverite"
                    selected={this.state.selectedTab === 'Faverite'}
                    onPress={() => this.onChangeTab('Faverite')}
                />

                <TabBar.Item
                     icon={<Icon type='material-community' name='history' />}
                    title="Booking History"
                    selected={this.state.selectedTab === 'Booking'}
                    onPress={() => this.onChangeTab('Booking')}
                />


                <TabBar.Item
                    icon={<Icon type='material-community' name='account' />}
                    title="Profile"
                    selected={this.state.selectedTab === 'Profile'}
                    onPress={onClickProfile}
                />
            </TabBar>

        );
    }
}

export default listmovies

const styles = StyleSheet.create({
    content: {
        flex: 1,
        // alignItems: 'center',
        // justifyContent: 'center'
    },
    imagesize: {
        width: 50,
        height: 50,
        marginLeft: 10

    },
    center: {
        alignItems: 'center',
        //justifyContent: 'center'
    },
    button: {
        backgroundColor: '#f47373',
        borderColor: '#f47373'
    },
    textHead: {
        textAlign: 'center',
        fontSize: 15,
        color: 'black',

    },
    textmovie: {
        textAlign: 'center',
        fontSize: 15,
        color: 'white',

    },
    boxHeader: {
        flexDirection: 'row',
        backgroundColor: '#f5f5f5',
        borderBottomWidth: 3,
        borderBottomColor: '#f47373',
        justifyContent: 'center',
        padding: 15,
    },
    boxContent: {
        flexDirection: 'column',
        // backgroundColor: 'white',
        // justifyContent: 'center',
        //margin: 10

    },
    rowHeader: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
    rowIcon: {
        flex: 0,
    },
    box1: {
        flex: 1,
        //height: 250,

    },
    box2: {
        flex: 1,
        height: 250,
    },
    header: {
        flex: 1,
        flexDirection: 'row',
        height: 250,
        width: '100%',
    },


});