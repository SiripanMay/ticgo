import React, { Component } from 'react';
import { Text, View, StyleSheet, Image, TouchableOpacity, ScrollView, ListItem, ActivityIndicator } from 'react-native';
import { SearchBar, TabBar, Tabs, List, Flex, Grid, Button, InputItem } from '@ant-design/react-native';
import { Icon } from 'react-native-elements'
import axios from 'axios'
import { push } from 'connected-react-router'
import { connect } from 'react-redux'
import ImagePicker from 'react-native-image-picker'

class editProfile extends Component {

    onClickShowing = () => {
        this.props.history.push('/listmovies')

    }
    onClickProfile = () => {
        this.props.history.push('/profile')

    }

    onClickSave = () => {
        const { user } = this.props
        console.log(user[0].user.token);


        axios({
            url: 'https://zenon.onthewifi.com/ticGo/users',
            method: 'put',
            headers: {
                'Authorization': `Bearer ${user[0].user.token}`
            },
            data: {
                firstName: this.state.firstName,
                lastName: this.state.lastName
            }
        }).then(() => {
            return axios.get('https://zenon.onthewifi.com/ticGo/users', {
                headers: {
                    'Authorization': `Bearer ${user[0].user.token}`
                },
            }
            )
        })
            .then(response => {
                console.log('response', response);

                this.props.saveUserInRedux(response.data.user);
                this.props.history.push('/profile')
            }).catch(e => {
                console.log("error " + e)
            })



    }


    constructor(props) {
        super(props);
        this.state = {
            selectedTab: 'Profile',
            items: [],
            isLoading: true,
            email: '',
            password: '',
            firstName: '',
            lastName: '',
            imagePath: '',
            loadImage: false,

        };
    }

    onChangeTab() {
        this.setState({
            selectedTab: tabName,
        });
    }


    selectImage = () => {
        const { user } = this.props
        ImagePicker.showImagePicker({}, (response) => {
            console.log(response)
            if (response.didCancel) {
                console.log('User cancelled image picker');
            } else if (response.error) {
                console.log('ImagePicker Error: ', response.error);
            } else if (response.customButton) {
                console.log('User tapped custom button: ', response.customButton);
            } else if (response.uri) {
                const formData = new FormData()
                this.setState({ loadImage: true })
                formData.append('image', {
                    uri: response.uri,
                    name: response.fileName,
                    type: response.type,
                })
                axios.post('https://zenon.onthewifi.com/ticGo/users/image', formData, {
                    headers: {
                        Authorization: `Bearer ${user[0].user.token}`
                    },
                    onUploadProgress: progressEvent => {
                        
                        console.log('progress', Math.floor(progressEvent.loaded / progressEvent.total * 100))                     
                    }
                })
                    .then(response => {
                        this.setState({
                            imagePath: response.data.image,
                            loadImage: false
                        })
                    })
                    .catch(error => {
                        console.log(error.response)
                    })

            }
        })
    }

    componentDidMount() {
        this.getImage()
    }

    getImage = () => {
        const { user } = this.props
        axios.get('https://zenon.onthewifi.com/ticGo/users', {
            headers: {
                Authorization: `Bearer ${user[0].user.token}`
            }
        })
            .then(response => {
                this.setState({
                    firstName: response.data.user.firstName,
                    lastName: response.data.user.lastName,
                    imagePath: response.data.user.image,
                    isLoading: false
                })


            })
            .catch(err => { console.log('data', err) })
            .finally(() => { console.log('Finally') })
    }


    render() {
        const { user } = this.props
        console.log(user)
        console.log(this.state.items);
        console.log(this.state.loadImage);




        return (


            <TabBar
                unselectedTintColor="#949494"
                tintColor="#f47373"
                barTintColor="#f5f5f5"
            >
                <TabBar.Item
                    title="Showing"
                    icon={<Icon type='material-community' name='youtube' />}
                    //selected={this.state.selectedTab === 'Showing'}
                    onPress={() => this.onClickShowing()}
                />


                <TabBar.Item
                    icon={<Icon type='material-community' name='heart' />}
                    title="Faverite"
                //selected={this.state.selectedTab === 'Faverite'}
                //onPress={this.onClickFaverite}
                />

                <TabBar.Item
                    icon={<Icon type='material-community' name='history' />}
                    title="Booking History"
                //selected={this.state.selectedTab === 'Booking'}
                //onPress={() => this.onChangeTab('Booking')}
                />


                <TabBar.Item
                    icon={<Icon type='material-community' name='account' />}
                    title="Profile"
                    selected={this.state.selectedTab === 'Profile'}
                    onPress={() => this.onClickProfile()}
                >
                    <View style={[styles.content]}>
                        <View style={[styles.boxColumn]}>
                            <View style={[styles.boxHeader]}>
                                <TouchableOpacity
                                    onPress={() => this.onClickProfile()}
                                >
                                    <View style={[styles.rowIcon]}>
                                        <Icon type='material-community' name='chevron-left' />
                                    </View>
                                </TouchableOpacity>
                                <View style={[styles.rowHeader]}>
                                    <Text style={[styles.textHead]}>Edit Profile</Text>
                                </View>
                            </View>

                            <View style={[styles.box1, styles.center]}>

                                <TouchableOpacity onPress={this.selectImage} style={[styles.img]}>
                                    <View style={{ position: 'absolute' }}>
                                        <Icon
                                            raised
                                            name='camera'
                                            type='font-awesome'
                                            color='#f47373'
                                        />

                                    </View>


                                    {this.state.loadImage === false ? (
                                        <Image
                                            style={{ width: '100%', height: '100%', borderRadius: 100 }}
                                            source={{ uri: this.state.imagePath }}
                                        />
                                    ) : (
                                            <ActivityIndicator size="large" color="#f47373" />

                                        )}

                                </TouchableOpacity>
                            </View>

                            <View style={[styles.box2, styles.center]}>
                                <Text>{this.state.loadImage}</Text>
                                <Text>{this.state.firstName}-{this.state.lastName}</Text>
                                <InputItem
                                    style={{ color: 'black' }}
                                    clear
                                    value={this.state.firstName}
                                    onChangeText={value => { this.setState({ firstName: value }) }}
                                    placeholder="First Name"
                                />
                                <InputItem
                                    style={{ color: 'black' }}
                                    clear
                                    value={this.state.lastName}
                                    onChangeText={value => { this.setState({ lastName: value }) }}
                                    placeholder="Last Name"
                                />


                                {/* <Text style={[styles.textStyle]}>First name : {user[0].user.firstName}</Text>
                                <Text style={[styles.textStyle]}>Last name : {user[0].user.lastName}</Text> */}


                                <View style={{ flexDirection: 'row' }}>

                                    <Button type='primary' style={[styles.button]} onPress={() => this.onClickSave()} activeStyle={{ backgroundColor: '#f47373' }}>Save</Button>
                                </View>
                            </View>

                        </View>
                    </View>
                </TabBar.Item>
            </TabBar>

        );
    }
}
const mapStateToProps = (state) => {
    return {
        user: state.user
    }
}
const mapDispatchToProps = (dispatch) => {
    return {
        saveUserInRedux: ({ firstName, lastName }) => {
            dispatch({
                type: 'EDIT_USER',
                firstName: firstName,
                lastName: lastName
            })
        }
    }
}


export default connect(mapStateToProps, mapDispatchToProps)(editProfile)

const styles = StyleSheet.create({
    content: {
        flex: 1,
        // alignItems: 'center',
        // justifyContent: 'center'
    },
    img: {
        backgroundColor: 'white',
        borderRadius: 100,
        width: 200,
        height: 200,
    },

    center: {
        alignItems: 'center',
        justifyContent: 'center'
    },
    button: {
        backgroundColor: '#f47373',
        borderColor: '#f47373',
        margin: 10
    },
    textHead: {
        textAlign: 'center',
        fontSize: 15,
        color: 'black',
    },
    boxHeader: {
        flexDirection: 'row',
        backgroundColor: '#f5f5f5',
        borderBottomWidth: 3,
        borderBottomColor: '#f47373',
        justifyContent: 'center',
        padding: 15,


    },
    boxContent: {
        //flexDirection: 'row',
        backgroundColor: 'white',
        justifyContent: 'center',
        margin: 10

    },
    rowHeader: {
        flex: 1,
    },
    boxColumn: {
        flex: 1,
        backgroundColor: '#f5f5f5',
        flexDirection: 'column'
    },
    box1: {
        flex: 1,

    },
    box2: {
        flex: 1,
    },
    textStyle: {
        fontSize: 20
    }
});