import React, { Component } from 'react';
import { StyleSheet, ScrollView, Text, View, Image, TouchableOpacity ,Alert} from 'react-native';
import { Button, Flex, WhiteSpace, WingBlank, InputItem } from '@ant-design/react-native';
import axios from 'axios';


class signup extends Component {

  state = {
    visible: false,
    email: '',
    password: '',
    firstname: '',
    lastname: '',
  }

  onSignup = () => {
    this.setState({ visible: true })
    axios({
      url: 'https://zenon.onthewifi.com/ticGo/users/register?email',
      method: 'post',
      data: {
        email: this.state.email,
        password: this.state.password,
        firstName: this.state.firstname,
        lastName: this.state.lastname
      }
    }).then(response => {
      const { data } = response
      const { user } = data
      this.props.history.push('/signin')
    }).catch(e => {
      console.log("error ", e.response)
      this.setState({ visible: false })
    })
  }

  onClickBack = () => {
    this.props.history.push('/signin')
  }

  render() {


    console.log(this.props);

    return (
      <View style={styles.content}>

        <WingBlank style={{ marginBottom: 5 }}>


          <Flex style={[styles.center]}>
            <Image source={require('./images/logo.png')} style={[styles.imagesize]} />
          </Flex>
          <Text style={styles.textHead}>SIGN UP</Text>
          <InputItem
            type="text"
            placeholder="First name"
            onChange={firstname => {
              this.setState({
                firstname,
              });
            }}
          />

          <InputItem
            type="text"
            placeholder="Last name"
            onChange={lastname => {
              this.setState({
                lastname,
              });
            }}
          />

          <InputItem
            type="email"
            placeholder="Email"
            value={this.state.email}
            onChange={email => {
              this.setState({
                email,
              });
            }}
          />

          <InputItem
            type="password"
            placeholder="Password"
            value={this.state.password}
            onChange={password => {
              this.setState({
                password,
              });
            }}
          />

          <WingBlank style={{ margin: 20 }}>
            <Button 
            loading={this.state.visible}
            type='primary' style={[styles.button]} 
            activeStyle={{ backgroundColor: '#f47373' }}
            onPress={() => this.onSignup()}>SIGN UP </Button>

            <TouchableOpacity onPress={this.onClickBack}>
              <Text style={{ textAlign: 'center', fontSize: 15, margin: 10, color: '#3f51b5' }}>Back to Sign In</Text>
            </TouchableOpacity>
          </WingBlank>
        </WingBlank>
      </View>
    );
  }
}

export default signup

const styles = StyleSheet.create({
  content: {
    flex: 1,
    // alignItems: 'center',
    justifyContent: 'center'
  },
  imagesize: {
    width: 200,
    height: 200
  },
  center: {
    alignItems: 'center',
    justifyContent: 'center'
  },
  button: {
    backgroundColor: '#f47373',
    borderColor: '#f47373'
  },
  textHead: {
    textAlign: 'center',
    fontSize: 25,
    color: 'black',
    marginBottom: 10
  },
});