import React, { Component } from 'react';
import { Text, View, StyleSheet, Image, TouchableOpacity, ScrollView, ActivityIndicator, ListItem } from 'react-native';
import { SearchBar, TabBar, Tabs, List, Flex, Grid } from '@ant-design/react-native';
import { Icon } from 'react-native-elements'
import axios from 'axios'

class listmovies extends Component {

  onClickSignup = () => {
    this.props.history.push('/signup')
  }
  onClickMovie = (item) => {
    this.props.history.push('/detailBooking', { item: item })

  }
  onClickFaverite = () => {
    this.props.history.push('/faverite')

  }
  onClickShowing= () => {
    this.props.history.push('/listmovies')

  }
  onClickProfile= () => {
    this.props.history.push('/profile')

  }

  onClickBookingHistory = () => {
    this.props.history.push('/bookingHistory')

}
  constructor(props) {
    super(props);
    this.state = {
      selectedTab: 'Showing',
      items: [],
      isLoading: true,
    };
  }



  onChangeTab() {
    this.setState({
      selectedTab: tabName,
    });
  }

  componentDidMount() {
    this.getMovie()
  }

  getMovie = () => {
    axios.get('https://zenon.onthewifi.com/ticGo/movies')
      .then(response => {
        this.setState({
          items: response.data,
          isLoading: false
        })
      })
      .catch(err => { console.log(err) })
      .finally(() => { console.log('Finally') })
  }


  render() {

    onClickProfile = () => {
      this.props.history.push('/signin')
    }
    return (


      <TabBar
        unselectedTintColor="#949494"
        tintColor="#f47373"
        barTintColor="#f5f5f5"
      >
        <TabBar.Item
          title="Showing"
          icon={<Icon type='material-community' name='youtube' />}
          selected={this.state.selectedTab === 'Showing'}
          onPress={()=>this.onClickShowing()}
        >
          <View style={[styles.contents]}>

            <View style={[styles.boxHeader]}>

              <View style={[styles.rowHeader]}>
                <Text style={[styles.textHead]}>Showing</Text>
              </View>

            </View>
            <ScrollView>
              {!this.state.isLoading ? (
                <View style={[styles.boxContent]}>

                  <Grid
                    data={this.state.items}
                    columnNum={2}
                    renderItem={(item) => (
                      <TouchableOpacity
                        onPress={() => this.onClickMovie(item)}
                      >
                        <Image source={{ uri: item.image }}
                          style={{ width: '100%', height: 250 }} />
                        <Text style={[styles.textHead]}>{item.name}</Text>
                      </TouchableOpacity>
                    )}
                    itemStyle={{ height: 270, margin: 10 }}
                  />

                </View>



              ) : (

                  <ActivityIndicator size="large" color="#0000ff" style={[styles.center]} />

                )}
              <View style={{padding: 30}}>


                <View style={[styles.rowHeader]}>
                </View>

              </View>
            </ScrollView>

          </View>
        </TabBar.Item>



        <TabBar.Item
          icon={<Icon type='material-community' name='heart' />}
          title="Faverite"
          //selected={this.state.selectedTab === 'Faverite'}
          //onPress={this.onClickFaverite}
        />

        <TabBar.Item
          icon={<Icon type='material-community' name='history' />}
          title="Booking History"
          //selected={this.state.selectedTab === 'Booking'}
          onPress={() => this.onClickBookingHistory()}
        />


        <TabBar.Item
          icon={<Icon type='material-community' name='account' />}
          title="Profile"
          //selected={this.state.selectedTab === 'Profile'}
         onPress={()=>this.onClickProfile()}
        />
      </TabBar>

    );
  }
}

export default listmovies

const styles = StyleSheet.create({
  content: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center'
  },
  imagesize: {
    width: '100%',
    height: 250,
    marginLeft: 10

  },
  center: {
    alignItems: 'center',
    justifyContent: 'center'
  },
  button: {
    backgroundColor: '#f47373',
    borderColor: '#f47373'
  },
  textHead: {
    textAlign: 'center',
    fontSize: 15,
    color: 'black',
  },
  boxHeader: {
    flexDirection: 'row',
    backgroundColor: '#f5f5f5',
    borderBottomWidth: 3,
    borderBottomColor: '#f47373',
    justifyContent: 'center',
    padding: 15,


  },
  boxContent: {
    //flexDirection: 'row',
    backgroundColor: 'white',
    justifyContent: 'center',
    margin: 10

  },
  rowHeader: {
    flex: 1,
  },
  rowIcon: {
    flex: 0,
  }
});