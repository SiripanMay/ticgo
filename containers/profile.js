import React, { Component } from 'react';
import { Text, View, StyleSheet, Image, TouchableOpacity, ScrollView, ActivityIndicator, ListItem } from 'react-native';
import { SearchBar, TabBar, Tabs, List, Flex, Grid, Button, InputItem } from '@ant-design/react-native';
import { Icon } from 'react-native-elements'
import axios from 'axios'
import { push } from 'connected-react-router'
import { connect } from 'react-redux'
import ImagePicker from 'react-native-image-picker'

class profile extends Component {

    onClickShowing = () => {
        this.props.history.push('/listmovies')

    }
    onClickProfile = () => {
        this.props.history.push('/profile')

    }
    onClickEdit = () => {
        this.props.history.push('/editProfile')

    }

    onClickLogout = () => {
        this.props.history.push('/signin')

    }
    onClickBookingHistory = () => {
        this.props.history.push('/bookingHistory')

    }


    constructor(props) {
        super(props);
        this.state = {
            selectedTab: 'Profile',
            items: [],
            isLoading: true,
            email: '',
            password: '',
            firstname: '',
            lastname: '',
            imagePath: '',
            loadImage: true

        };
    }

    onChangeTab() {
        this.setState({
            selectedTab: tabName,
        });
    }

    componentDidMount() {
        this.getImage()
    }

    getImage = () => {
        const { user } = this.props
        axios.get('https://zenon.onthewifi.com/ticGo/users', {
            headers: {
                Authorization: `Bearer ${user[0].user.token}`
            }
        })
            .then(response => {
                console.log(response);

                this.setState({
                    email: response.data.user.email,
                    firstname: response.data.user.firstName,
                    lastname: response.data.user.lastName,
                    imagePath: response.data.user.image,
                    loadImage: false
                })


            })
            .catch(err => { console.log(err) })
            .finally(() => { console.log('Finally') })
    }


    render() {
        const { user } = this.props
        console.log(user)
        console.log(this.state.firstname);
        console.log(this.state.lastname);



        return (


            <TabBar
                unselectedTintColor="#949494"
                tintColor="#f47373"
                barTintColor="#f5f5f5"
            >
                <TabBar.Item
                    title="Showing"
                    icon={<Icon type='material-community' name='youtube' />}
                    //selected={this.state.selectedTab === 'Showing'}
                    onPress={() => this.onClickShowing()}
                />


                <TabBar.Item
                    icon={<Icon type='material-community' name='heart' />}
                    title="Faverite"
                //selected={this.state.selectedTab === 'Faverite'}
                //onPress={this.onClickFaverite}
                />

                <TabBar.Item
                    icon={<Icon type='material-community' name='history' />}
                    title="Booking History"
                    //selected={this.state.selectedTab === 'Booking'}
                    onPress={() => this.onClickBookingHistory()}
                />


                <TabBar.Item
                    icon={<Icon type='material-community' name='account' />}
                    title="Profile"
                    selected={this.state.selectedTab === 'Profile'}
                    onPress={() => this.onClickProfile()}
                >
                    <View style={[styles.content]}>
                        <View style={[styles.boxColumn]}>
                            <View style={[styles.boxHeader]}>
                                <View style={[styles.rowHeader]}>
                                    <Text style={[styles.textHead]}>Profile</Text>
                                </View>
                            </View>

                            <View style={[styles.box1, styles.center]}>
                                <View style={[styles.img]}>


                                    {this.state.loadImage === false ? (
                                        <Image
                                            style={{ width: '100%', height: '100%', borderRadius: 100 }}
                                            source={{ uri: this.state.imagePath }}
                                        />
                                    ) : (
                                            <ActivityIndicator size="large" color="#f47373" />

                                        )}

                                </View>
                            </View>

                            <View style={[styles.box2, styles.center]}>
                                <Text style={[styles.textStyle]}>First name : {this.state.firstname}</Text>
                                <Text style={[styles.textStyle]}>Last name : {this.state.lastname}</Text>
                                <Text style={[styles.textStyle]}>Email : {this.state.email}</Text>
                                <View style={{ flexDirection: 'row' }}>
                                    <Button type='primary' style={[styles.button]} onPress={() => this.onClickEdit()} activeStyle={{ backgroundColor: '#f47373' }}>Edit</Button>
                                    <Button type='primary' style={[styles.button]} onPress={() => this.onClickLogout()} activeStyle={{ backgroundColor: '#f47373' }}>Logout</Button>
                                </View>
                            </View>

                        </View>
                    </View>
                </TabBar.Item>
            </TabBar>

        );
    }
}
const mapStateToProps = (state) => {
    return {
        user: state.user
    }
}

export default connect(mapStateToProps)(profile)

const styles = StyleSheet.create({
    content: {
        flex: 1,
        // alignItems: 'center',
        // justifyContent: 'center'
    },
    img: {
        backgroundColor: 'white',
        borderRadius: 100,
        width: 200,
        height: 200,
    },

    center: {
        alignItems: 'center',
        justifyContent: 'center'
    },
    button: {
        backgroundColor: '#f47373',
        borderColor: '#f47373',
        margin: 10
    },
    textHead: {
        textAlign: 'center',
        fontSize: 15,
        color: 'black',
    },
    boxHeader: {
        flexDirection: 'row',
        backgroundColor: '#f5f5f5',
        borderBottomWidth: 3,
        borderBottomColor: '#f47373',
        justifyContent: 'center',
        padding: 15,


    },
    boxContent: {
        //flexDirection: 'row',
        backgroundColor: 'white',
        justifyContent: 'center',
        margin: 10

    },
    rowHeader: {
        flex: 1,
    },
    boxColumn: {
        flex: 1,
        backgroundColor: '#f5f5f5',
        flexDirection: 'column'
    },
    box1: {
        flex: 1,

    },
    box2: {
        flex: 1,
    },
    textStyle: {
        fontSize: 20
    }
});