import React, { Component } from 'react';
import { Text, View, StyleSheet, Image, TouchableOpacity, ScrollView, ActivityIndicator, ListItem, FlatList } from 'react-native';
import { SearchBar, TabBar, Tabs, List, Flex, Grid, Button, InputItem, Card, WingBlank } from '@ant-design/react-native';
import { Icon } from 'react-native-elements'
import axios from 'axios'
import { push } from 'connected-react-router'
import { connect } from 'react-redux'

class profile extends Component {

    onClickShowing = () => {
        this.props.history.push('/listmovies')

    }
    onClickProfile = () => {
        this.props.history.push('/profile')

    }
    onClickEdit = () => {
        this.props.history.push('/editProfile')

    }

    onClickLogout = () => {
        this.props.history.push('/signin')

    }
    onClickBookingHistory = () => {
        this.props.history.push('/bookingHistory')

    }

    constructor(props) {
        super(props);
        this.state = {
            selectedTab: 'Booking',
            items: [],
            isLoading: true,
            email: '',
            password: '',
            firstname: '',
            lastname: '',
            imagePath: '',
            historys: [],
            tickets: [],
            ticket: []

        };
    }

    onChangeTab() {
        this.setState({
            selectedTab: tabName,
        });
    }

    componentDidMount() {
        this.getImage()
    }

    getImage = () => {
        const { user } = this.props
        axios.get('https://zenon.onthewifi.com/ticGo/movies/book', {
            headers: {
                Authorization: `Bearer ${user[0].user.token}`
            }
        }).then(response => {
            const data = response.data
            this.setState({ tickets: data, isLoading: false })
            for (let index = 0; index < data.length; index++) {
                //console.log('Ticket INdex:', data);
                axios.get(`https://zenon.onthewifi.com/ticGo/movies/showtimes/${data[index].showtime}`)
                    .then(response => {
                        this.setState({ ticket: [...this.state.ticket, response.data] })
                        console.log('ticket:', this.state.ticket);
                         console.log('ticketS:', this.state.tickets);

                    })
            }
        })

            .catch(err => { console.log(err) })
            .finally(() => { console.log('Finally') })
    }

    showDateTime = (item) => {
        var date = new Date(item.startDateTime).toDateString()
        var time = new Date(item.startDateTime).toLocaleTimeString().replace(/(.*)\D\d+/, '$1')
        return `${date} ${time}`
    }
    seatPrint = (seats) => {
        const seat = []
        
        
        for (let index = 0; index < seats.length; index++) {
            seat.push(<Text style={{fontSize:12}}>{seats[index].type} Column: {seats[index].column} Row: {seats[index].row}</Text>)
        }
        
        return seat
    }

    _keyExtractor = (item, index) => {
        console.log('rrrrrrrrrrr',item);
        
       // item._id
    }




    render() {
        const { user } = this.props
        // console.log(user)
        // console.log(this.state.historys);

        return (


            <TabBar
                unselectedTintColor="#949494"
                tintColor="#f47373"
                barTintColor="#f5f5f5"
            >
                <TabBar.Item
                    title="Showing"
                    icon={<Icon type='material-community' name='youtube' />}
                    //selected={this.state.selectedTab === 'Showing'}
                    onPress={() => this.onClickShowing()}
                />


                <TabBar.Item
                    icon={<Icon type='material-community' name='heart' />}
                    title="Faverite"
                //selected={this.state.selectedTab === 'Faverite'}
                //onPress={this.onClickFaverite}
                />

                <TabBar.Item
                    icon={<Icon type='material-community' name='history' />}
                    title="Booking History"
                    selected={this.state.selectedTab === 'Booking'}
                    onPress={() => this.onClickBookingHistory()}
                >
                    <View style={[styles.content]}>

                        <View style={[styles.boxColumn]}>
                            <View style={[styles.boxHeader]}>
                                <View style={[styles.rowHeader]}>
                                    <Text style={[styles.textHead]}>Booking History</Text>
                                </View>
                            </View>


                            <View style={[styles.box1]}>
                                <ScrollView>
                                    {!this.state.isLoading?(
                                    <FlatList
                                        inverted data={this.state.ticket}
                                        keyExtractor={this._keyExtractor}
                                        renderItem={(item, index) => (
                                            <WingBlank size="lg">
                                                <TouchableOpacity>
                                                    <Card style={{ margin: 5 }}>
                                                        <Card.Header style={{ fontSize: 10 }}
                                                            title={item.item.movie.name}
                                                            thumbStyle={{ width: 30, height: 30 }}
                                                            extra={this.showDateTime(item.item)}
                                                        />
                                                        <Card.Body >
                                                            <View style={{ flex:1,height:120 }}>
                                                            <View style={{ flexDirection:'row' }}>
                                                            <View style={{ flex:0,alignItems:'center',justifyContent:'center',margin:10}}>
                                                            <Image source={{ uri: item.item.movie.image }}
                                                                    style={{ width: 70, height: 110 }} />
                                                            </View>
                                                            <View style={{ flex:1,alignItems:'center',justifyContent:'center'}}>
                                                            
                                                            {this.seatPrint(this.state.tickets[item.index].seats)}  
                                                            <Text style={{fontSize:12,color:'#1A237E'}}>Booked Date: {new Date(this.state.tickets[item.index].createdDateTime).toDateString()}</Text>                                                         
                                                            </View>
                                                                
                                                            </View>        
                                                            </View>
                                                        </Card.Body>

                                                    </Card>
                                                </TouchableOpacity>
                                            </WingBlank>

                                        )
                                        }
                                    />
                                    ):(<ActivityIndicator size="large" color="#f47373" />)}
                                </ScrollView>
                            </View>
                        </View>
                    </View>


                </TabBar.Item>
                <TabBar.Item
                    icon={<Icon type='material-community' name='account' />}
                    title="Profile"
                    selected={this.state.selectedTab === 'Profile'}
                    onPress={() => this.onClickProfile()}
                />


            </TabBar>

        );
    }
}
const mapStateToProps = (state) => {
    return {
        user: state.user
    }
}

export default connect(mapStateToProps)(profile)

const styles = StyleSheet.create({
    content: {
        flex: 1,
        // alignItems: 'center',
        // justifyContent: 'center'
    },
    img: {
        backgroundColor: 'white',
        borderRadius: 100,
        width: 200,
        height: 200,
    },

    center: {
        alignItems: 'center',
        justifyContent: 'center'
    },
    button: {
        backgroundColor: '#f47373',
        borderColor: '#f47373',
        margin: 10
    },
    textHead: {
        textAlign: 'center',
        fontSize: 15,
        color: 'black',
    },
    boxHeader: {
        flexDirection: 'row',
        backgroundColor: '#f5f5f5',
        borderBottomWidth: 3,
        borderBottomColor: '#f47373',
        justifyContent: 'center',
        padding: 15,


    },
    boxContent: {
        //flexDirection: 'row',
        backgroundColor: 'white',
        justifyContent: 'center',
        margin: 10

    },
    rowHeader: {
        flex: 1,
    },
    boxColumn: {
        flex: 1,
        backgroundColor: '#f5f5f5',
        flexDirection: 'column'
    },
    box1: {
        flex: 1,
        backgroundColor: 'pink'

    },
    box2: {
        flex: 1,
    },
    textStyle: {
        fontSize: 20
    }
});