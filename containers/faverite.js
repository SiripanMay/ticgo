import React, { Component } from 'react';
import { Text, View, StyleSheet, Image, TouchableOpacity, ScrollView, ActivityIndicator, ListItem } from 'react-native';
import { Icon, SearchBar, TabBar, Tabs, List, Flex, Grid } from '@ant-design/react-native';
import axios from 'axios'

class listmovies extends Component {

  onClickSignup = () => {
  this.props.history.push('/signup')
  }
  onClickMovie = (movie) => {
    this.props.history.push('/detailBooking',{name:movie})

  }

  renderContent(pageText) {
    return (
      <View style={{ flex: 1, marginLeft: 10, backgroundColor: 'pink' }}>
        <Icon name="left" size="md" color="red" />
        <Image source={require('./images/logo.png')} style={styles.imagesize} />
      </View>
    );
  }

  constructor(props) {
    super(props);
    this.state = {
      selectedTab: 'Faverite',
      items: [],
      isLoading: true,
    };
  }



  onChangeTab() {
    this.setState({
      selectedTab: tabName,
    });
  }

  componentDidMount() {
    this.getMovie()
  }

  getMovie = () => {
    axios.get('https://zenon.onthewifi.com/ticGo/movies')
      .then(response => {
        this.setState({
          items: response.data,
          isLoading: false
        })
      })
      .catch(err => { console.log(err) })
      .finally(() => { console.log('Finally') })
  }


  render() {

    const tabs = [
      { title: 'First Tab' }
    ];
    const style = {
      alignItems: 'center',
      justifyContent: 'center',
      height: 200,
      backgroundColor: '#fff',
    };

    onClickProfile = () => {
      this.props.history.push('/signin')
    }
    return (


      <TabBar
        unselectedTintColor="#949494"
        tintColor="#f47373"
        barTintColor="#f5f5f5"
      >
        <TabBar.Item
          title="Showing"
          icon={<Icon name="youtube" />}
          selected={this.state.selectedTab === 'Showing'}
          onPress={() => this.onChangeTab('Showing')}
        >
          <View style={[styles.contents]}>

            <View style={[styles.boxHeader]}>

              {/* <TouchableOpacity>
              <View style={[styles.rowIcon]}>
                <Icon name="left" size="md" color="black" />
              </View>
              </TouchableOpacity> */}

              <View style={[styles.rowHeader]}>
                <Text style={[styles.textHead]}>Showing</Text>
              </View>

            </View>
            <ScrollView>
              {!this.state.isLoading ? (
                <View style={[styles.boxContent]}>

                  <Grid
                    data={this.state.items}
                    columnNum={2}
                    renderItem={(item) => (
                      <TouchableOpacity
                      onPress={()=>this.onClickMovie(item.name)}
                      >
                        <Image source={{ uri: item.image }}
                          style={{ width: '100%', height: '100%' }} 
                          
                          />      
                        </TouchableOpacity>       
                    )}
                    itemStyle={{ height: 270, margin: 10 }}
                  />

                </View>



              ) : (

                  <ActivityIndicator size="large" color="#0000ff" style={[styles.center]} />

                )}

            </ScrollView>
          </View>
        </TabBar.Item>



        <TabBar.Item
          icon={<Icon name="heart" />}
          title="Faverite"
          selected={this.state.selectedTab === 'Faverite'}
          onPress={() => this.onChangeTab('Faverite')}
        />

        <TabBar.Item
          icon={<Icon name="reload-time" />}
          title="Booking History"
          selected={this.state.selectedTab === 'Booking'}
          onPress={() => this.onChangeTab('Booking')}
        />


        <TabBar.Item
          icon={<Icon name="user" />}
          title="Profile"
          selected={this.state.selectedTab === 'Profile'}
          onPress={onClickProfile}
        />
      </TabBar>

    );
  }
}

export default listmovies

const styles = StyleSheet.create({
  content: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center'
  },
  imagesize: {
    width: 50,
    height: 50,
    marginLeft: 10

  },
  center: {
    alignItems: 'center',
    justifyContent: 'center'
  },
  button: {
    backgroundColor: '#f47373',
    borderColor: '#f47373'
  },
  textHead: {
    textAlign: 'center',
    fontSize: 15,
    color: 'black',
  },
  boxHeader: {
    flexDirection: 'row',
    backgroundColor: '#f5f5f5',
    borderBottomWidth: 3,
    borderBottomColor: '#f47373',
    justifyContent: 'center',
    padding: 15,


  },
  boxContent: {
    //flexDirection: 'row',
    backgroundColor: 'white',
    justifyContent: 'center',
    margin: 10

  },
  rowHeader: {
    flex: 1,
  },
  rowIcon: {
    flex: 0,
  }
});