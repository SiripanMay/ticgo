import React, { Component } from 'react'
import { ConnectedRouter } from 'connected-react-router'
import { Route, NativeRouter, Switch, Redirect } from 'react-router-native'
import { Provider } from 'react-redux'
import { store, history } from './store';
import signin from './containers/signin'
import signup from './containers/signup'
import listmovies from './containers/listmovies'
import detailBooking from './containers/detailBooking'
import faverite from './containers/faverite'
import cinema from './containers/cinema'
import profile from './containers/profile'
import editProfile from './containers/editProfile'
import bookingHistory from './containers/bookingHistory'
class Router extends Component {
    render() {
        return (
            <Provider store={store}>
                <ConnectedRouter history={history}>
                    <Switch>
                        <Route exact path='/signin' component={signin} />
                        <Route exact path='/signup' component={signup} />
                        <Route exact path='/listmovies' component={listmovies} />
                        <Route exact path='/detailBooking' component={detailBooking} />
                        <Route exact path='/faverite' component={faverite} />
                        <Route exact path='/cinema' component={cinema} />
                        <Route exact path='/profile' component={profile} />
                        <Route exact path='/editProfile' component={editProfile} />
                        <Route exact path='/bookingHistory' component={bookingHistory} />
                        <Redirect to="/signin" />
                    </Switch>
                </ConnectedRouter>
            </Provider>

        )
    }
}
export default Router